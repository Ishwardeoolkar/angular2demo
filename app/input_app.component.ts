import {Component} from "angular2/core"

@Component({
	selector: 'my-input',
	template: `<div class="row">
		<div class="col-md-4 col-lg-4 card-list">
			<div class="col-md-12 col-lg-12 card-white card-shadow card-padding">
				<h3>My Favourite is : {{myname}}</h3>
				<ul>
					<li *ngFor="#name of names">{{name}}</li>
				</ul>
			</div>
		</div>
		<div class="col-md-4 col-lg-4 card-margin card-ul-list">
			<div class="col-md-12 col-lg-12 card-white card-shadow card-padding">
				<h3>Todo List</h3>
				<ul>
					<li *ngFor="#Item of Items" (click)="onItemClicked(Item)">{{Item.name}}</li>
				</ul>
				<input type="text" #Item class="form-control input-lg card-margin" placeholder="Please Enter the Items....."/>
				<div class="row">
					<div class="col-md-6">
						<button class="btn btn-block btn-success btn-md" (click)="onAddItem(Item)">ADD ITEM</button>
					</div>
					<div class="col-md-6">
						<button class="btn btn-block btn-danger btn-md" (click)="ondeleteItem()">DELETE ITEM</button>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-lg-4 text-center">
			<div class="col-md-12 col-lg-12 card-white card-shadow card-padding">
				<h3>Displaying Data in Angular 2</h3>
				<h4>{{player}}</h4>
				<p>{{sport}}</p>
				<h4>{{capital}}</h4>
				<h4>Data Binding</h4>
				<input type="text" [(ngModel)]="clickedItem.name" class="form-control input-lg">
				<p>{{clickedItem.name}}</p>
			</div>
		</div>
	</div>`

})
export class InputComponent{
	public Items = [
		{name:'Butter'},
		{name:'Curd'},
		{name:'Milk'},
		{name:'Lassi'}
	];
	public clickedItem = {name: ""};
	onItemClicked(Item){
		this.clickedItem = Item;
	}

	onAddItem(Item){
		this.Items.push({name:Item.value});
	}
	ondeleteItem(){
		this.Items.splice(this.Items.indexOf(this.clickedItem),1);
	}
	player: 'MS-Dhoni';
	sport: 'Cricket';

	capital:string;
	constructor() {
		this.capital= 'Mumbai';
	}

	names = ['Ishwar','Dattatrey','Pavan','Pranali','Vaishanvi','Supriya'];
	myname = this.names[1];

}