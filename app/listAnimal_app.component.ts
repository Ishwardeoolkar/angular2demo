import {Component, View} from "angular2/core";

@Component({
	selector:'my-animal'
})

@View({
	template:`<div class="row">
		<div class="col-md-4 col-lg-4"></div>
		<div class="col-md-4 col-lg-4 card-white card-ul-list card-shadow ">
			<ul>
				<li *ngFor="#myAnimal of AnimalList">{{myAnimal.name}}</li>
			</ul>
		</div>
		<div class="col-md-4 col-lg-4"></div>
	</div>`
})

export class AnimalComponent{
	public AnimalList = [
		{name:'Lion'},
		{name:'Tiger'},
		{name:'Elephant'},
		{name:'Camel'},
		{name:'Giraffc'},
		{name:'Dear'},
		{name:'rabit'},
	];
}