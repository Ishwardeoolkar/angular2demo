System.register(["angular2/core"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var ItemComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            ItemComponent = (function () {
                function ItemComponent() {
                    this.itemList = [
                        { name: 'Apple', image: '../img/apple.jpg', nickname: 'Apple' },
                        { name: 'Orange', image: '../img/orange.jpg', nickname: 'Orange' },
                        { name: 'Grapes', image: '../img/grapes.jpg', nickname: 'Grapes' },
                        { name: 'Bannana', image: '../img/bannana.jpg', nickname: 'Bannana' },
                        { name: 'WaterLemon', image: '../img/waterlemon.jpg', nickname: 'WaterLemon' },
                        { name: 'Pineapple', image: '../img/pine.jpg', nickname: 'Pineapple' },
                        { name: 'JackFruit', image: '../img/jackfruit.png', nickname: 'JackFruit' },
                        { name: 'Chickoo', image: '../img/chickoo.jpg', nickname: 'Chickoo' },
                    ];
                }
                ItemComponent = __decorate([
                    core_1.Component({
                        selector: 'mylist',
                        template: "<h2 class=\"text-center\">List of Fruits</h2>\n\t<div class=\"row\">\n\t\t<div *ngFor=\"#myitem of itemList\" class=\"col-md-3 card-margin\">\n\t\t\t<div class=\"col-md-12 col-lg-12 card-image no-padding card-white card-shadow card-heading\">\n\t\t\t\t<img src=\"{{myitem.image}}\" alt={{myitem.nickname}}/>\n\t\t\t\t<div class=\"col-md-12 col-lg-12\">\n\t\t\t\t\t<h4>{{myitem.name}}</h4>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>"
                    }), 
                    __metadata('design:paramtypes', [])
                ], ItemComponent);
                return ItemComponent;
            }());
            exports_1("ItemComponent", ItemComponent);
        }
    }
});
//# sourceMappingURL=itemComponent_app.component.js.map