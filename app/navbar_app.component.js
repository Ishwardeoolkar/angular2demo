System.register(["angular2/core"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var NavComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            NavComponent = (function () {
                function NavComponent() {
                }
                NavComponent = __decorate([
                    core_1.Component({
                        selector: 'my-nav'
                    }),
                    core_1.View({
                        template: "<nav class=\"navbar navbar-inverse card-navbar card-shadow\">\n\t  \t<div class=\"container-fluid\">\n\t    \t<div class=\"navbar-header\">\n\t      \t\t<a class=\"navbar-brand\" href=\"#\">Angular 2</a>\n\t    \t</div>\n\t    \t<ul class=\"nav navbar-nav\">\n\t      \t\t<li class=\"active\"><a href=\"#\">Home</a></li>\n\t      \t\t<li><a href=\"#\">Page 1</a></li>\n\t      \t\t<li><a href=\"#\">Page 2</a></li>\n\t    \t</ul>\n\t    \t<ul class=\"nav navbar-nav navbar-right\">\n\t      \t\t<li><a href=\"#\"><span class=\"glyphicon glyphicon-user\"></span>&nbsp; Sign Up</a></li>\n\t      \t\t<li><a href=\"#\"><span class=\"glyphicon glyphicon-log-in\"></span>&nbsp; Login</a></li>\n\t    \t</ul>\n\t  \t</div>\n\t</nav>"
                    }), 
                    __metadata('design:paramtypes', [])
                ], NavComponent);
                return NavComponent;
            }());
            exports_1("NavComponent", NavComponent);
        }
    }
});
//# sourceMappingURL=navbar_app.component.js.map