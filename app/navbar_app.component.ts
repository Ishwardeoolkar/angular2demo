import {Component, View} from "angular2/core";

@Component({
   selector: 'my-nav'
})

@View({
  template: `<nav class="navbar navbar-inverse card-navbar card-shadow">
	  	<div class="container-fluid">
	    	<div class="navbar-header">
	      		<a class="navbar-brand" href="#">Angular 2</a>
	    	</div>
	    	<ul class="nav navbar-nav">
	      		<li class="active"><a href="#">Home</a></li>
	      		<li><a href="#">Page 1</a></li>
	      		<li><a href="#">Page 2</a></li>
	    	</ul>
	    	<ul class="nav navbar-nav navbar-right">
	      		<li><a href="#"><span class="glyphicon glyphicon-user"></span>&nbsp; Sign Up</a></li>
	      		<li><a href="#"><span class="glyphicon glyphicon-log-in"></span>&nbsp; Login</a></li>
	    	</ul>
	  	</div>
	</nav>`
})

export class NavComponent {

}
