System.register(["angular2/platform/browser", "./navbar_app.component", "./hello_app.component", "./metadata_app.component", "./input_app.component", "./listAnimal_app.component", "./keyup_app.component", "./loopback_app.component", "./form_app.component"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var browser_1, navbar_app_component_1, hello_app_component_1, metadata_app_component_1, input_app_component_1, listAnimal_app_component_1, keyup_app_component_1, loopback_app_component_1, form_app_component_1;
    return {
        setters:[
            function (browser_1_1) {
                browser_1 = browser_1_1;
            },
            function (navbar_app_component_1_1) {
                navbar_app_component_1 = navbar_app_component_1_1;
            },
            function (hello_app_component_1_1) {
                hello_app_component_1 = hello_app_component_1_1;
            },
            function (metadata_app_component_1_1) {
                metadata_app_component_1 = metadata_app_component_1_1;
            },
            function (input_app_component_1_1) {
                input_app_component_1 = input_app_component_1_1;
            },
            function (listAnimal_app_component_1_1) {
                listAnimal_app_component_1 = listAnimal_app_component_1_1;
            },
            function (keyup_app_component_1_1) {
                keyup_app_component_1 = keyup_app_component_1_1;
            },
            function (loopback_app_component_1_1) {
                loopback_app_component_1 = loopback_app_component_1_1;
            },
            function (form_app_component_1_1) {
                form_app_component_1 = form_app_component_1_1;
            }],
        execute: function() {
            browser_1.bootstrap(navbar_app_component_1.NavComponent);
            browser_1.bootstrap(hello_app_component_1.HelloComponent);
            browser_1.bootstrap(metadata_app_component_1.MyTemplate);
            browser_1.bootstrap(input_app_component_1.InputComponent);
            browser_1.bootstrap(listAnimal_app_component_1.AnimalComponent);
            browser_1.bootstrap(keyup_app_component_1.keyupComponent);
            browser_1.bootstrap(loopback_app_component_1.loopbackComponent);
            browser_1.bootstrap(form_app_component_1.formComponent);
        }
    }
});
//# sourceMappingURL=environment_main.js.map