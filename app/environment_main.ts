import {bootstrap} from "angular2/platform/browser"
import {NavComponent} from "./navbar_app.component"
import {HelloComponent} from "./hello_app.component"
import {MyTemplate} from "./metadata_app.component"
import {InputComponent} from "./input_app.component"
import {AnimalComponent} from "./listAnimal_app.component"
import {keyupComponent} from "./keyup_app.component"
import {loopbackComponent} from "./loopback_app.component"
import {formComponent} from "./form_app.component"

bootstrap(NavComponent);
bootstrap(HelloComponent);
bootstrap(MyTemplate);
bootstrap(InputComponent);
bootstrap(AnimalComponent);
bootstrap(keyupComponent);
bootstrap(loopbackComponent);
bootstrap(formComponent);