System.register(["angular2/core"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var InputComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            InputComponent = (function () {
                function InputComponent() {
                    this.Items = [
                        { name: 'Butter' },
                        { name: 'Curd' },
                        { name: 'Milk' },
                        { name: 'Lassi' }
                    ];
                    this.clickedItem = { name: "" };
                    this.names = ['Ishwar', 'Dattatrey', 'Pavan', 'Pranali', 'Vaishanvi', 'Supriya'];
                    this.myname = this.names[1];
                    this.capital = 'Mumbai';
                }
                InputComponent.prototype.onItemClicked = function (Item) {
                    this.clickedItem = Item;
                };
                InputComponent.prototype.onAddItem = function (Item) {
                    this.Items.push({ name: Item.value });
                };
                InputComponent.prototype.ondeleteItem = function () {
                    this.Items.splice(this.Items.indexOf(this.clickedItem), 1);
                };
                InputComponent = __decorate([
                    core_1.Component({
                        selector: 'my-input',
                        template: "<div class=\"row\">\n\t\t<div class=\"col-md-4 col-lg-4 card-list\">\n\t\t\t<div class=\"col-md-12 col-lg-12 card-white card-shadow card-padding\">\n\t\t\t\t<h3>My Favourite is : {{myname}}</h3>\n\t\t\t\t<ul>\n\t\t\t\t\t<li *ngFor=\"#name of names\">{{name}}</li>\n\t\t\t\t</ul>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"col-md-4 col-lg-4 card-margin card-ul-list\">\n\t\t\t<div class=\"col-md-12 col-lg-12 card-white card-shadow card-padding\">\n\t\t\t\t<h3>Todo List</h3>\n\t\t\t\t<ul>\n\t\t\t\t\t<li *ngFor=\"#Item of Items\" (click)=\"onItemClicked(Item)\">{{Item.name}}</li>\n\t\t\t\t</ul>\n\t\t\t\t<input type=\"text\" #Item class=\"form-control input-lg card-margin\" placeholder=\"Please Enter the Items.....\"/>\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\t<div class=\"col-md-6\">\n\t\t\t\t\t\t<button class=\"btn btn-block btn-success btn-md\" (click)=\"onAddItem(Item)\">ADD ITEM</button>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"col-md-6\">\n\t\t\t\t\t\t<button class=\"btn btn-block btn-danger btn-md\" (click)=\"ondeleteItem()\">DELETE ITEM</button>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"col-md-4 col-lg-4 text-center\">\n\t\t\t<div class=\"col-md-12 col-lg-12 card-white card-shadow card-padding\">\n\t\t\t\t<h3>Displaying Data in Angular 2</h3>\n\t\t\t\t<h4>{{player}}</h4>\n\t\t\t\t<p>{{sport}}</p>\n\t\t\t\t<h4>{{capital}}</h4>\n\t\t\t\t<h4>Data Binding</h4>\n\t\t\t\t<input type=\"text\" [(ngModel)]=\"clickedItem.name\" class=\"form-control input-lg\">\n\t\t\t\t<p>{{clickedItem.name}}</p>\n\t\t\t</div>\n\t\t</div>\n\t</div>"
                    }), 
                    __metadata('design:paramtypes', [])
                ], InputComponent);
                return InputComponent;
            }());
            exports_1("InputComponent", InputComponent);
        }
    }
});
//# sourceMappingURL=input_app.component.js.map