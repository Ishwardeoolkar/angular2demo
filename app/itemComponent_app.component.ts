import {Component} from "angular2/core";

@Component({
	selector:'mylist',
	template:`<h2 class="text-center">List of Fruits</h2>
	<div class="row">
		<div *ngFor="#myitem of itemList" class="col-md-3 card-margin">
			<div class="col-md-12 col-lg-12 card-image no-padding card-white card-shadow card-heading">
				<img src="{{myitem.image}}" alt={{myitem.nickname}}/>
				<div class="col-md-12 col-lg-12">
					<h4>{{myitem.name}}</h4>
				</div>
			</div>
		</div>
	</div>`
})

export class ItemComponent{
	public itemList = [
		{name:'Apple',image:'../img/apple.jpg',nickname:'Apple'},
		{name:'Orange',image:'../img/orange.jpg',nickname:'Orange'},
		{name:'Grapes',image:'../img/grapes.jpg',nickname:'Grapes'},
		{name:'Bannana',image:'../img/bannana.jpg',nickname:'Bannana'},
		{name:'WaterLemon',image:'../img/waterlemon.jpg',nickname:'WaterLemon'},
		{name:'Pineapple',image:'../img/pine.jpg',nickname:'Pineapple'},
		{name:'JackFruit',image:'../img/jackfruit.png',nickname:'JackFruit'},
		{name:'Chickoo',image:'../img/chickoo.jpg',nickname:'Chickoo'},
	];
}