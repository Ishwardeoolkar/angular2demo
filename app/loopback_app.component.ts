import {Component} from "angular2/core";

@Component({
	selector : 'loop-back',
	template : `<div class="row">
		<div class="col-lg-5 col-md-5">
			<h2>Local Template Variable</h2>
			<input type="text" class="form-control" #key_val (keyup)="0"/>
			<!-- The key_val variable is a reference to the <input> element itself, and grab the input element's value and display it with interpolation between <p> tags -->
			<p>{{key_val.value}}</p>
		</div>
	</div>`
})

export class loopbackComponent{
	
}