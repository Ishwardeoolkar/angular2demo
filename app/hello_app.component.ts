import {Component, View} from "angular2/core";

@Component({
   selector: 'my-div'
})
@View({
  template: '<h1 class="text-center">Welcome to Angular 2</h1>'
})

export class HelloComponent {

}