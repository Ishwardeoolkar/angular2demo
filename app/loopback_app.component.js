System.register(["angular2/core"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var loopbackComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            loopbackComponent = (function () {
                function loopbackComponent() {
                }
                loopbackComponent = __decorate([
                    core_1.Component({
                        selector: 'loop-back',
                        template: "<div class=\"row\">\n\t\t<div class=\"col-lg-5 col-md-5\">\n\t\t\t<h2>Local Template Variable</h2>\n\t\t\t<input type=\"text\" class=\"form-control\" #key_val (keyup)=\"0\"/>\n\t\t\t<!-- The key_val variable is a reference to the <input> element itself, and grab the input element's value and display it with interpolation between <p> tags -->\n\t\t\t<p>{{key_val.value}}</p>\n\t\t</div>\n\t</div>"
                    }), 
                    __metadata('design:paramtypes', [])
                ], loopbackComponent);
                return loopbackComponent;
            }());
            exports_1("loopbackComponent", loopbackComponent);
        }
    }
});
//# sourceMappingURL=loopback_app.component.js.map