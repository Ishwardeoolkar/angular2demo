System.register(["angular2/core"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var AnimalComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            AnimalComponent = (function () {
                function AnimalComponent() {
                    this.AnimalList = [
                        { name: 'Lion' },
                        { name: 'Tiger' },
                        { name: 'Elephant' },
                        { name: 'Camel' },
                        { name: 'Giraffc' },
                        { name: 'Dear' },
                        { name: 'rabit' },
                    ];
                }
                AnimalComponent = __decorate([
                    core_1.Component({
                        selector: 'my-animal'
                    }),
                    core_1.View({
                        template: "<div class=\"row\">\n\t\t<div class=\"col-md-4 col-lg-4\"></div>\n\t\t<div class=\"col-md-4 col-lg-4 card-white card-ul-list card-shadow \">\n\t\t\t<ul>\n\t\t\t\t<li *ngFor=\"#myAnimal of AnimalList\">{{myAnimal.name}}</li>\n\t\t\t</ul>\n\t\t</div>\n\t\t<div class=\"col-md-4 col-lg-4\"></div>\n\t</div>"
                    }), 
                    __metadata('design:paramtypes', [])
                ], AnimalComponent);
                return AnimalComponent;
            }());
            exports_1("AnimalComponent", AnimalComponent);
        }
    }
});
//# sourceMappingURL=listAnimal_app.component.js.map