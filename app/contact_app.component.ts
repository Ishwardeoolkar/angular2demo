import {Component} from "angular2/core";
import {ngForm} from "angular2/common";
import {Contact} from "./contact";

@Component({
	selector : 'contact-form',
	templateUrl :'app/form_app.component.html'
})
export class ContactComponent{
	countries = ['India', 'Australia', 'England', 'Sri-Lankha', 'Real Madrid', 'Assam', 'kolaphur'];
	contact = new Contact('Ravi','Sharma',this.countries[0],9090078789);
	submitted = false;
	onSubmit() {this.submitted:true;}
	active = true;
	newContact(){
		this.contact = new Contact('','');
		this.active = false;
		setTimeout(()=> this.active=true,0);
	}
	console.log(contact);
}