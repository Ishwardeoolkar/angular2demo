import {Component} from "angular2/core";
import {ItemComponent} from "./itemComponent_app.component";

@Component({
	selector: 'my-item',
	template:`<mylist></mylist>`,
	directives:[ItemComponent]
})
export class MyTemplate{
	
}