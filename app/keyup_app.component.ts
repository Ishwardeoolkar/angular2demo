import {Component} from "angular2/core";

@Component({
	selector:'my-key',
	template : `<div class="row">
		<div class="col-md-5 col-lg-5">
			<h2>Keyup User Input Event Binding</h2>
			<input ype="text" (keyup)="onKey($event)" class="form-control input-lg" placeholder="Enter the value..."/>
			<p>{{ val }}</p>
		</div>
	</div>`
})

export class keyupComponent{
	val = '';
	onKey(event:keyboardevnt){
		this.val += (event.target).value + ' | ';
		console.log(this.val);
	}
}
